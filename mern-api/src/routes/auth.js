const express = require('express')
const router = express.Router()

const authoController = require('../controllers/auth')

router.post('/register',authoController.register)
router.post('/login',authoController.Login)

module.exports = router
