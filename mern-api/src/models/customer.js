const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Customer = new Schema({

    name:{
        type:String,
        required:true   
    },
    address:{
        type:String,
        required:true
    },
    age:{
        type:String,
        required:true
    }

},{
    timestamps : true   
})

module.exports = mongoose.model('Customer',Customer)