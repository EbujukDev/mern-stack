const {validationResult} = require('express-validator')
const BlogPost  = require('../models/blog')
const path = require('path')
const fs = require('fs')
const excelToJson = require('convert-excel-to-json');
const privateKey = "$2b$10$ehug.w.4asdasEHnmtyj394rZUugasdasd/1231y8jyBasdasdjQM1Kz/HWhfdow/x/T012p1cW"
const jwt = require('jsonwebtoken');

exports.createBlogPost = (req, res, next) => {

    const error = validationResult(req);

    var decoded = jwt.verify(req.body.token, privateKey);

    console.log(decoded)
    if(!decoded){
       const err = new Error('Token tidak ditemukan');
       err.errorStatus = 400
       err.data = error.array()
       throw err
    }
    
    if(!error.isEmpty()){
       const err = new Error('Invalid Value');
       err.errorStatus = 400
       err.data = error.array()
       throw err
    }

    if(!req.file){
        const err = new Error('Image harus diupload');
        err.errorStatus = 422
        throw err
    }

    const excelData = excelToJson({
        sourceFile: req.file.path,
        sheets:[{
            // Excel Sheet Name
            name: 'Customers',
 
            // Header Row -> be skipped and will not be present at our result object.
            header:{
               rows: 1
            },
			
            // Mapping columns to keys
            columnToKey: {
                A: '_id',
                B: 'name',
                C: 'address',
                D: 'age'
            }
        }]
    });
 
    // -> Log Excel Data to Console
    console.log(excelData);
 
    const title = req.body.title
    const image = req.file.path
    const body  = req.body.body

    const Posting = new BlogPost({
        title: title,
        body: body,
        image:image,
        author:{
            name:'Alif Prasetyo Aji',
            uid:1
        }
    })

    console.log(Posting)

    
    Posting.save().then(result => {
        res.status(201).json({
            message:'Created Blog Success',
            data:result
        })
    })
    .catch(err =>{
        console.log('err:',err)
    })
}

exports.getAllBlogPost = (req, res, next) =>{

    const currentPage = req.query.page || 1
    const perPage = req.query.perPage  || 5
    let totalItems;

    BlogPost.find()
    .countDocuments()
    .then(count => {
        totalItems = count

        return BlogPost.find()
        .skip((parseInt(currentPage) - 1) * parseInt(perPage))
        .limit(parseInt(perPage))         

    })
    .then(result => {
        res.status(200).json({
            message: 'Data Blog Post Berhasil',
            data: result,
            total_data : parseInt(totalItems),
            per_page : parseInt(perPage),
            current_page : parseInt(currentPage)
        })
    })
    .catch(err => {
        next(err)
    })
}

exports.getBlogPostById = (req, res, next) => {
    BlogPost.findById(req.params.postId)
    .then(result => {
        if(!result){
            const error = new Error('Blog Post tidak ditemukan')
            error.errorStatus = 404
            throw error
        }

        res.status(200).json({
            message:'Data Berhasil ditemukan',
            data:result
        })

    })
    .catch(err => {
        next(err)
    }) 
}

exports.updateBlogPost = (req, res, next) => {
    const error = validationResult(req);

    if(!error.isEmpty()){
       const err = new Error('Invalid Value');
       err.errorStatus = 400
       err.data = error.array()
       throw err
    }

    if(!req.file){
        const err = new Error('Image harus diupload');
        err.errorStatus = 422
        throw err
    }


    const title = req.body.title
    const image = req.file.path
    const body  = req.body.body
    const postId = req.params.postId

    BlogPost.findById(postId)
    .then(post => {
        if(!post){
            const err = new Error('Data tidak ditemukan')
            err.errorStatus = 401
            throw err
        }

        post.title = title
        post.body  = body
        post.image = image

        return post.save();
    })
    .then(result => {
        res.status(200).json({
            message:'update sukses',
            data:result
        })
    })
    .catch(err => {
        next(err)
    })
}

exports.deleteBlogPost = (req, res, next) => {

    const postId = req.params.postId

    BlogPost.findById(postId)
    .then(post => {
        if(!post){
            const err = new Error('Data tidak ditemukan')
            err.errorStatus = 404
            throw err
        }

        clearImage(post.image)
        return BlogPost.findByIdAndRemove(postId)
        

    })
    .then(result => {
        res.status(200).json({
            message:'Delete berhasil',
            data: result
        })
    })
    .catch(err => {
        next(err)
    })

}

const clearImage = (filePath) => {
    console.log('Path : ',filePath)
    console.log('Direktori:',__dirname)
    
    filePath = path.join(__dirname,'../..',filePath)
    fs.unlink(filePath, err => console.log(err))
}
 