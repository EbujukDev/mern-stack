const register = require("../models/register")
const bcrypt = require('bcrypt')
const Auth  = require('../models/register')
const jwt = require('jsonwebtoken');

exports.register = (req, res, next) =>{
    const name = req.body.name
    const email = req.body.email
    const salt = bcrypt.genSaltSync(10)
    const password = bcrypt.hashSync(req.body.password,salt)
    
   const createUser = new register({
       name:name,
       email:email,
       password:password
   })

   createUser.save().then(response =>{
       res.status(201).json({
           message:'Register Success',
           data: response
       })
   }).catch(err =>{
      console.log('err:',err)
      })

}

exports.Login = (req, res, next) => {

    const password = req.body.password
    const privateKey = "$2b$10$ehug.w.4asdasEHnmtyj394rZUugasdasd/1231y8jyBasdasdjQM1Kz/HWhfdow/x/T012p1cW"
    
    console.log(req.body.email)

    Auth.findOne({email:req.body.email})
    .then(result =>{

        const validPassword = bcrypt.compareSync(password,result.password)
        if(!validPassword)
            res.status(401).json({
                message:'Password Tidak Valid',
                data:null
            })

            var token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60),
                data: result
            }, privateKey);
            var decoded = jwt.verify(token, privateKey);
            console.log(decoded)

            res.status(201).json({
                message:'Login Berhasil',
                data:result,
                token: token
            })
      
    })
    .catch(err => {
        res.status(401).json({
            message:'Password Tidak Valid',
            data:null
        })
    })

}