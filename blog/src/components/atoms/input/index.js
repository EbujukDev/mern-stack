import React from 'react'
import './input.scss'
function Input({label,type, ...rest}) {
    return (
        <div className="input-wrapper">
            <p className="label">{label}</p>
            <>
            { type === 'password' ? (
                <input className="input" {...rest} type="password" />

            ):(
                <input className="input" {...rest}  />

            )}
            </>
        </div>
    )
}

export default Input
