import React from 'react'

function Gap({width,height}) {
    return (
        <div style={{height,width}} />
    )
}

export default Gap
