import React from 'react'
import { Link, useHistory, withRouter } from 'react-router-dom'
import './header.scss'
const Header = () => {
    const history = useHistory();
    const clearLocalStorage = () =>{
        localStorage.clear();
        history.push('/login')
    }
    return (
        <div className="header">
            <div>
                <div className="list-menu">
                    <div>
                        <Link style={{textdecoration:'none'}} to="/" onClick={() => window.location.reload()} className="logo-app">
                            MERN Blog
                        </Link>
                    </div>
                    {/* <div className="list-dropdown">
                        <p>Berita</p>
                        <p>Beranda</p>
                    </div> */}
                </div>
                
            </div>
            <div>
                <p className="menu-item" onClick={clearLocalStorage}>Logout</p>
            </div>
            
        </div>
    )
}

export default withRouter(Header)
