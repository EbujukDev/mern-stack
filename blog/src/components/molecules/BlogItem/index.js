import React from 'react'
import { Link, useHistory } from 'react-router-dom'
import ReactHtmlParser from 'react-html-parser';
import './blogitem.scss'
const BlogItem = ({id,img,title,author,body}) => {
    const history = useHistory();
    return (
        <div className="blog-item" >
            <img className="image-thumb" src={img} alt="Logo" />
            <div className="content-detail">
                <p className="title">{title}</p>
                <p className="author">{author}</p>
                <p className="body">{ReactHtmlParser(body.slice(0,200))}.
                <Link style={{ textDecoration: 'none' }} onClick={()=>history.push(`/detail-blog/${id}`)}> Baca Selengkapnya ...</Link> 
                </p>
            </div>
        </div>
    )
}

export default BlogItem
