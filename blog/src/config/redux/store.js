const {createStore, combineReducers} = require('redux')

const initializeHome = {
    dataBlog : []
}
const initializeName = {
    name : "Maman Mimin"
}

const globalHome = (state = initializeHome, action) =>{
    switch (action.type) {
        case 'GET_DATA_GLOBAL':
            
            return {
                ...state,
                dataBlog:action.payload
            }
    
        default:
            return {
                ...state,
            }
    }
}

const globalName = (state = initializeName, action) =>{
    switch (action.type) {
        case 'GET_DATA_GLOBAL_NAME':
            
            return{
                ...state,
                name:'Budi Sudarsono'
            }
    
        default:
            return{
                ...state,
            }
    }
}

const reducer = combineReducers({globalHome,globalName})
const store = createStore(reducer)

export default store