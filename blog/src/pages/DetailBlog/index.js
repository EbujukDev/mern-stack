import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useHistory, withRouter } from 'react-router-dom'
import { Button } from '../../components'
import ReactHtmlParser from 'react-html-parser';
import './detailBlog.scss'
const DetailBlog = (props) => {
    const[detailBlog,setDetailBlog] = useState([])
    useEffect(() => {
        const _id = props.match.params.id
        
        // axios.get(`http://localhost:4000/v1/blog/post/${_id}`)
        axios.get(`http://192.168.1.208:4000/v1/blog/post/${_id}`)
        .then(result => {
            setDetailBlog(result.data.data)        
        })
        .catch(err => console.log(err))
    })
    const history = useHistory();
    return (
        <div className="detail-blog-wrapper">
            {/* <img className="img-cover" src={`http://localhost:4000/${detailBlog.image}`} alt="logo" /> */}
            <img className="img-cover" src={`http://192.168.1.208:4000/${detailBlog.image}`} alt="logo" />

            <p className="blog-title">{detailBlog.title}</p>
            <p className="blog-author">Alif Prasetyo Aji - 12 Desember 2022</p>
            <p className="blog-body">{ReactHtmlParser(detailBlog.body)}</p>
            <div className="button-action">
                <Button title="Kembali" onClick={()=>history.push('/')} />
            </div>
        </div>
    )
}

export default withRouter(DetailBlog) 
