import axios from 'axios'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { Button, Gap, Input, TextArea, Upload } from '../../components'
import { useToasts } from 'react-toast-notifications';

import './createBlog.scss'
const CreateBlog = () => {

    const [title, setTitle] = useState('')
    const [body, setBody] = useState('')
    const [image, setImage] = useState('')
    const [imagePreview, setImagePreview] = useState('')
    const { addToast } = useToasts();

    const uploadImages = (e) => {
        const file = e.target.files[0]
        setImage(file)
        setImagePreview(URL.createObjectURL(file))
    }

    const onSubmit = () => {
        
        console.log(body)

        const formData = new FormData()

        formData.append('title',title)
        formData.append('body',body)
        formData.append('image',image)
        formData.append('token',JSON.parse(localStorage.getItem('token')))
        // console.log(JSON.parse(localStorage.getItem('token')))

        axios.post('http://localhost:4000/v1/blog/post',formData)
        .then(response => {
            console.log(response)
            addToast('Create Successfully', { appearance: 'success',autoDismiss:true });

        })
        .catch(err => {
            console.log(err)
            addToast('Create Failed', { appearance: 'error',autoDismiss:true });

        })

    }

    const history = useHistory();
    return (
        <div className="blog-post">
            <p className="title">Create New Blog</p>
            <Input label="Post Title" placeholder="example: Bahaya Es krim di siang hari saat puasa" onChange={(e) => setTitle(e.target.value)} />
            <Input label="Author " placeholder="example: Alif Prasetyo Aji - 12 November 2021 "  />

            <Upload onChange={(e) => uploadImages(e)} img={imagePreview} />
            <TextArea onChange={(e) => setBody(e.level.content)} />
            <Gap height={10} />
            <div className="button-action">
                <Button title="Kembali" onClick={()=>history.push('/')} />
                <Gap width={10} />
                <Button title="Create" onClick={onSubmit} />
            </div>
        </div>
    )
}

export default CreateBlog
