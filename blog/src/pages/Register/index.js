import React from 'react'
import { RegisterBg } from '../../asssets'
import { Button, Input } from '../../components'
import './register.scss'
const Register = () => {
    return (

        <div className="main-page">
            <div className="left-page">
                <img src={RegisterBg} className="bg-image" alt="logo" />
            </div>
            <div className="right-page">
                <p>Register Page</p>
                <Input label="Full Name" placeholder="Full Name" />
                <Input label="Email" placeholder="Email" />
                <Input label="Password" placeholder="Password" />
                <Button title="Register"></Button>
            </div>
        </div>
        
    )
}

export default Register
