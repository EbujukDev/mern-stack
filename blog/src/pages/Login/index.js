import axios from 'axios'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { useToasts } from 'react-toast-notifications';
import { RegisterBg } from '../../asssets'
import { Button, Input } from '../../components'
const Login = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const history = useHistory();
    const { addToast } = useToasts();

    const onSubmit = () => {
        const formLogin = new FormData()

        formLogin.append('email',email)
        formLogin.append('password',password)

        // axios.post('http://localhost:4000/v1/auth/login',formLogin,{ "Content-Type": "multipart/form-data" })
        axios.post('http://192.168.1.208:4000/v1/auth/login',formLogin,{ "Content-Type": "multipart/form-data" })

        .then(response => {


            const res = response.data.data
            localStorage.setItem('token', JSON.stringify(response.data.token));
            console.log(response.data.token)
            localStorage.setItem('user', JSON.stringify(res));
            addToast('Login Successfully', { appearance: 'success',autoDismiss:true });
            setTimeout(() => {
                history.push('/')
            }, 2000);
        })
        .catch(err => {
            console.log(err)
            addToast('Failed, Please Check Username or Password', { appearance: 'error',autoDismiss:true });
        })
    }

    return (
       
        <div className="main-page">
            <div className="left-page">
                <img src={RegisterBg} className="bg-image" alt="logo" />
            </div>
            <div className="right-page">
                <p style={{textAlign:'center',fontWeight:'bold',fontSize:'24px'}}>Login Page</p>
                <Input label="Email" placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
                <Input type="password" label="Password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                <div style={{justifyContent:'right',width:'200px',marginRight:'0'}}>
                <Button title="Login" onClick={onSubmit} />
                </div>
            </div>
        </div>
    )
}

export default Login
