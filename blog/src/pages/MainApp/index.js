import React,{ useEffect } from 'react'
import { BrowserRouter as Router, Route, Switch, useHistory } from 'react-router-dom'
import { Footer, Header } from '../../components'
import CreateBlog from '../CreateBlog'
import DetailBlog from '../DetailBlog'
import Home from '../Home'
import './mainApp.scss'



const MainApp = () => {
    
     const history = useHistory();
        
        useEffect(() => {
            if(!JSON.parse(localStorage.getItem('token'))){
                history.push('/login')
            }
        })
    
    return (
        <div className="main-app-wrapper">
            <div className="header-wrapper">
                <Header />
            </div>
            <div className="content-wrapper">
                <Router>
                    <Switch>
                        <Route exact path="/create-blog" component={CreateBlog} />
                        <Route exact path="/detail-blog/:id" component={DetailBlog}  />
                        <Route exact path="/" component={Home} />
                    </Switch>
                </Router>
            </div>
            <div className="footer-wrapper">
                <Footer />
            </div>
        </div>
    )
}

export default MainApp
