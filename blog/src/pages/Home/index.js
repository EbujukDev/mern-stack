import React, { useEffect, useState } from 'react'
import { BlogItem, Button, Gap } from '../../components'
import './home.scss'
import {useHistory, withRouter} from 'react-router-dom'
import axios from 'axios'
import { useDispatch, useSelector } from 'react-redux'

const Home = () => {
  const {dataBlog} = useSelector(state => state.globalHome);
  const dispatch = useDispatch();
  const [currentPage, setCurrentPage] = useState('')
  const [totalPage, settotalPage] = useState('')

  useEffect(() => {
      // axios.get('http://localhost:4000/v1/blog/posts',{params:{page:1}})
       axios.get('http://192.168.1.208:4000/v1/blog/posts',{params:{page:1}})

      .then(result => {
        dispatch({type:'GET_DATA_GLOBAL',payload:result.data.data})
        setCurrentPage(result.data.current_page)
        settotalPage(result.data.total_data)
      })
      .catch(err => console.log(err))


  },[dispatch])

  const nextPage = () => {

      console.log(totalPage)
      console.log(currentPage)
    if((parseInt(totalPage)/5) > currentPage){

      // axios.get('http://localhost:4000/v1/blog/posts',{params:{page:currentPage+1}})
      axios.get('http://192.168.1.208:4000/v1/blog/posts',{params:{page:currentPage+1}})

      .then(result => {
        dispatch({type:'GET_DATA_GLOBAL',payload:result.data.data})
        setCurrentPage(result.data.current_page)
        settotalPage(result.data.total_data)

      })
      .catch(err => console.log(err))
    }
  }
  

  const backPage = () => {

    // axios.get('http://localhost:4000/v1/blog/posts',{params:{page:currentPage-1}})
    axios.get('http://192.168.1.208:4000/v1/blog/posts',{params:{page:currentPage-1}})

    .then(result => {
      dispatch({type:'GET_DATA_GLOBAL',payload:result.data.data})
      setCurrentPage(result.data.current_page)

    })
    .catch(err => console.log(err))
  }


  
  const history = useHistory();
    return (
        <div className="home-page-wrapper">
          <div className="create-wrapper">
            <Button title="Create Blog" onClick={() => history.push('/create-blog')} />
          </div>
          <div className="content-wrapper">
            {dataBlog.map((blog) =>
            //img={`http://localhost:4000/${blog.image}`}
              <BlogItem key={blog._id} id={blog._id} title={blog.title} body={blog.body} img={`http://192.168.1.208:4000/${blog.image}`} author="Alif Prasetyo Aji - 12 Desember 2021" />
            )}
          </div>
          <div className="pagination"> 
              <Button title="Previous" onClick={backPage} />
              <Gap width={90} />
              <div style={{width:'100px;'}}>
              
               {/* {currentPage} / {Math.ceil((parseInt(totalPage)/5))} */}

              </div>
              <Gap width={30} />
              <Button title="Next" onClick={nextPage} />
              <Gap height={20}  />
          </div>
        </div>
    )
}

export default withRouter(Home)
